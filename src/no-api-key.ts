/* eslint-disable @typescript-eslint/explicit-function-return-type */
/* eslint-disable padding-line-between-statements */
/* eslint-disable func-style */
/* eslint-disable jsdoc/require-jsdoc */

import { browser } from "webextension-polyfill-ts";

function onClick() {
  try {
    if (chrome.runtime.openOptionsPage) {
      chrome.runtime.openOptionsPage();
    }
    if (browser.runtime.getURL) {
      const url = browser.runtime.getURL("/options.html");
      window.open(url);
    }
  } catch (error) {
    console.error(error);
    window.open('about:addons');
  }
};

function injectOptionsButton(parentElement: HTMLLIElement): void {
  const openButton = document.createElement("button");
  openButton.onclick = onClick;
  openButton.innerHTML = "Open Options";

  parentElement.append(openButton);
}

export { injectOptionsButton };

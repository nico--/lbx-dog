/* eslint-disable @typescript-eslint/explicit-function-return-type */
/* eslint-disable padding-line-between-statements */
/* eslint-disable func-style */
/* eslint-disable jsdoc/require-jsdoc */
/* eslint-disable sort-imports */

import { browser } from "webextension-polyfill-ts";

import { statsItem } from "./types/stats-item";
import { injectOptionsButton } from "./no-api-key";
import { getTopics, searchMedia } from "./api";
import { CategoryValues } from "./types/category-values";

/*
 * I'd prefer to use their window.filmData object, apparently extensions
 * scripts can't interact with page JS.
 * Might be possible to use weird semaphores, but this seems easier
 */
function getFilmData() {
  const ogTitle = document.querySelector<HTMLMetaElement>('meta[property="og:title"]')?.content;
  const titleTokens = ogTitle.split(" ");
  const yearWithParen = titleTokens.pop();
  const releaseYear = yearWithParen?.substr(1, yearWithParen.length - 2) ?? "";
  const title = encodeURIComponent(titleTokens.join(" "));

  return {
    releaseYear,
    title
  };
}

// eslint-disable-next-line func-style
// eslint-disable-next-line func-style

function injectTopics(parentElement: HTMLLIElement, topics: statsItem[] | void) {
  if (topics) {

    const topicList = document.createElement("ul");

    let numWarnings = 0;
    let numDealbreakers = 0;

    topics.forEach((t) => {
      const li = document.createElement("li");
      if (t.categoryPref === CategoryValues.warnPref) {
        li.style.color = '#ee7000'; // lbxd "pro" badge orange
        numWarnings += 1;
      } else if (t.categoryPref === CategoryValues.dealbreakerPref) {
        li.style.color = '#b00'; // lbxd blood-drip red
        li.style.fontWeight = "bold";
        numDealbreakers += 1;
      }
      const ratio = `${Math.trunc(100 * (t.yesSum / (t.yesSum + t.noSum)))}%`;
      li.innerHTML = `${t.topic.name} <i>${t.isVerifiedYes ? "(verified)" : `(${ratio})`}</i>`;
      topicList.append(li);
    });

    const detailElement = document.createElement("details");
    const summaryElement = document.createElement("summary");
    summaryElement.append(`${numWarnings} ⚠️, ${numDealbreakers} 🛑`);
    summaryElement.style.display = "list-item";
    parentElement.append(detailElement);
    detailElement.append(summaryElement);
    detailElement.append(topicList);
  } else {
    parentElement.append("None :)");
  }
}

function getExtensionsPanel(): HTMLLIElement | undefined {
  // find lbxd's pane
  const actionsPanel = document.getElementsByClassName("js-actions-panel")[0];
  if (!actionsPanel) {
    return;
  }

  // clean up any existing injection (relevant during hot-reloading in dev)
  const existingPanel = document.getElementsByClassName("panel-extensions")[0];
  if (existingPanel) {
    existingPanel.remove();
  }

  const extensionsPanel = document.createElement("li");
  extensionsPanel.className = "panel-extensions";
  actionsPanel.append(extensionsPanel);

  // eslint-disable-next-line consistent-return
  return extensionsPanel;
}

async function renderContentWarnings() {
  const extensionsPanel = getExtensionsPanel();
  if (!extensionsPanel) {
    return;
  }
  const title = document.createElement("h4");
  title.textContent = "Content Warnings:";
  title.style.fontWeight = "bold";
  extensionsPanel.append(title);

  const response = await browser.storage.sync.get("apiKey");
  const apiKey: string = response.apiKey ?? "";
  if (!apiKey) {
    injectOptionsButton(extensionsPanel);
  }

  const filmData = getFilmData();
  if (!filmData) {
    return;
  }

  const movie = await searchMedia(apiKey, filmData);

  if (movie) {
    const topics = await getTopics(apiKey, movie);
    injectTopics(extensionsPanel, topics);
  } else {
    extensionsPanel.append("<i>Movie has no DtDD page</i>");
  }
}

renderContentWarnings();

/* eslint-disable @typescript-eslint/explicit-function-return-type */
/* eslint-disable padding-line-between-statements */
/* eslint-disable func-style */
/* eslint-disable jsdoc/require-jsdoc */

import { statsItem } from "./types/stats-item";

function greaterRatioFirst(a: statsItem, b: statsItem): number {
  const aRatio = a.yesSum / a.noSum;
  const bRatio = b.yesSum / b.noSum;
  if (aRatio > bRatio) {
    // greater ratio first
    return -1;
  } else if (aRatio < bRatio) {
    return 1;
  }
  return 0;
}

function verifiedFirst(a: statsItem, b:statsItem): number {
  if (a.isVerifiedYes) {
    // verified at top
    return -1;
  }
  if (b.isVerifiedYes) {
    return 1;
  }
  return 0;
}

function preferenceFirst(a: statsItem, b:statsItem): number {
  if (a.categoryPref && b.categoryPref) {
    if (a.categoryPref < b.categoryPref) {
      return 1;
    } else if (a.categoryPref > b.categoryPref) {
      return -1;
    }
  }
  return 0;
}

export { greaterRatioFirst, preferenceFirst, verifiedFirst };

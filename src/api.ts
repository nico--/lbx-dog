/* eslint-disable @typescript-eslint/explicit-function-return-type */
/* eslint-disable padding-line-between-statements */
/* eslint-disable func-style */
/* eslint-disable jsdoc/require-jsdoc */
/* eslint-disable sort-imports */

import axios from "axios";
import { browser } from "webextension-polyfill-ts";
import "regenerator-runtime/runtime";

import { MediaItem } from "./types/media-item";
import { greaterRatioFirst, preferenceFirst, verifiedFirst } from "./stats-comparators";
import { CategoryValues } from "./types/category-values";
import { statsItem } from "./types/stats-item";

interface FilmData {title: string; releaseYear: string;}

function searchMedia(apiKey: string, filmData: FilmData) {
  const { title, releaseYear } = filmData;

  return axios.get(`https://www.doesthedogdie.com/dddsearch?q=${title}`, {
    headers: {
      "Accept": "application/json",
      "X-API-KEY": apiKey
    }
  })
  .then((res) => {
    const items: MediaItem[] = res?.data?.items ?? [];
    return items.find((i) => i.releaseYear === releaseYear);
  })
  .catch((err) => {
    console.error(err)
  });
}

function getTopics(apiKey: string, media: MediaItem) {
  return axios.get(`https://www.doesthedogdie.com/media/${media.id}`, {
    headers: {
      "Accept": "application/json",
      "X-API-KEY": apiKey
    }
  })
  .then(async (res) => {
    const items: statsItem[] = res?.data?.topicItemStats ?? [];
    const yesItems = items.filter((t) => t.isYes);
    const itemsWithPrefs = await Promise.all(yesItems.map(async (i) => {
      const valueInStorage = await browser.storage.sync.get(`cat-id-${i.topic.id}`);
      const categoryPref: number = Number.parseInt(
        valueInStorage[`cat-id-${i.topic.id}`] ?? CategoryValues.noPref, 10
      );

      return {
        ...i,
        categoryPref
      };
    }));
    console.log(itemsWithPrefs);
    const itemsFilteredToPrefs = itemsWithPrefs.filter(
      (i) => i.categoryPref !== CategoryValues.notBotheredPref
    );

    const itemsSortedByRatio = itemsFilteredToPrefs.sort(greaterRatioFirst);

    const itemsSortedByVerification = itemsSortedByRatio.sort(verifiedFirst);
    return itemsSortedByVerification.sort(preferenceFirst);

  })
  .catch((err) => {
    console.log(err);
  });
}

function getCategories(apiKey?: string) {
  const headers: any = {
    Accept: "application/json"
  };

  if (apiKey) {
    headers["X-API-KEY"] = apiKey;
  }

  return axios.get(`https://www.doesthedogdie.com/categories`, {
    headers
  })
  .then((res) => {
    return res.data ?? [];
  })
  .catch((err) => {
    console.error(err);
  });
}

export {
  searchMedia,
  getCategories,
  getTopics
};

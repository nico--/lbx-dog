/* eslint-disable @typescript-eslint/explicit-function-return-type */
/* eslint-disable padding-line-between-statements */
/* eslint-disable func-style */
/* eslint-disable jsdoc/require-jsdoc */
/* eslint-disable sort-imports */

import { browser } from "webextension-polyfill-ts";

import { getCategories } from "./api";
import { CategoryValues } from "./types/category-values";

function saveOption(e, key: string) {
  e.preventDefault();
  const el = document.querySelector(`#${key}`);
  if (el) {
    browser.storage.sync.set({
      [key]: el.value,
    });
  }
}

function initializeOption(key: string, valueInStorage: any) {
  const el = document.querySelector(`#${key}`);
  if (el) {
    el.value = valueInStorage[key] || "";
  }
}

function addRadioElement(
  parentElement: HTMLElement,
  categoryId: number,
  initialValue: number,
  key: number,
  displayName: string
): void {
    const radio = document.createElement("input");
     radio.type = "radio";
     radio.name = `cat-id-${categoryId}`;
     radio.id = `cat-id-${categoryId}-${CategoryValues[key]}-radio`;
     radio.value = key;
     radio.checked = initialValue === key;
     console.log(initialValue, key, initialValue === key);
     radio.addEventListener("change", (evt) => {
       console.log(evt);
      if (evt && evt.target) {
        browser.storage.sync.set({
          [evt.target.name]: evt.target.value as number
        });
      }
    });
    const radioLabel = document.createElement("label");
    radioLabel.htmlFor = `cat-id-${categoryId}-${CategoryValues[key]}-radio`;
    radioLabel.innerHTML = displayName;
    parentElement.append(radio);
    parentElement.append(radioLabel);

}

function renderCategories(categories: any[]) {
  const parentElement = document.querySelector("#categories");

  if (!parentElement) {
    return;
  }

  categories.forEach(async (category, index) => {
    const categoryElement = document.createElement("div");
    categoryElement.className = `zebra zebra-${index % 2 ? "odd" : "even"}`;
    const categoryTitle = document.createElement("h3");
    categoryElement.append(categoryTitle);
    categoryTitle.innerHTML = category.name;

    const valueInStorage = await browser.storage.sync.get(`cat-id-${category.id}`);
    const initialValue: number = parseInt(valueInStorage[`cat-id-${category.id}`] ?? CategoryValues.noPref, 10);
⚠️, ${numDealbreakers} 🛑

    addRadioElement(categoryElement, category.id, initialValue, CategoryValues.noPref, "No preference set (shown by default)");
    addRadioElement(categoryElement, category.id, initialValue, CategoryValues.notBotheredPref, "Hide warnings");
    addRadioElement(categoryElement, category.id, initialValue, CategoryValues.warnPref, "⚠️ Warn me about this");
    addRadioElement(categoryElement, category.id, initialValue, CategoryValues.dealbreakerPref, "🛑 This is a dealbreaker");

    parentElement.append(categoryElement);
  });
}

async function restoreOptions() {
  const response = await browser.storage.sync.get("apiKey");
  const apiKey: string = response.apiKey ?? "";
  if (!apiKey) {
    return;
  }

  initializeOption("apiKey", response);
  const categories = await getCategories(apiKey);
  renderCategories(categories);
}

document.addEventListener("DOMContentLoaded", restoreOptions);
document.querySelector("form").addEventListener("submit", (e) => saveOption(e, "apiKey"));

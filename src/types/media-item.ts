interface MediaItem {
    id: number;
    name: string;
    cleanName: string;
    genre: string;
    releaseYear: string;
    legacyId?: number;
    legacyUserId?: number;
    umId?: null;
    legacyItemType: "movie";
    newsletterDate: null;
    createdAt: string;
    updatedAt: string;
    UserId: number;
    ItemTypeId: number;
    tmdbId: number;
    imdbId?: number;
    backgroundImage: string;
    posterImage: string;
    tmdbResult?: string;
    overview?: string;
    itemType: {
      id: number;
      name: "Movie"
    };
    itemTypeId: number
}

export { MediaItem };

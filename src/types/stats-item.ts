import { CategoryValues } from "./category-values";

interface Topic {
  id: number;
  name: string;
  notName: string;
  keywords?: any;
  description?: string;
  subtitle?: string;
  subtitleText?: string;
  subtitleUrl?: string;
  doesName: string;
  listName: string;
  image: string;
  ordering: number;
  isSpoiler: boolean;
  isVisible: boolean;
  isSensitive: boolean;
  smmwDescription: string;
  legacyId?: number;
  createdAt: string;
  updatedAt: string;
}

interface Comment {
  id: number,
  voteSum: number,
  comment: string
  User: {
    id: number,
    displayName: string;
  }
}

interface statsItem {
  topic: Topic;
  topicItemId: number,
  newsletterDate?: string,
  yesSum: number,
  noSum: number,
  numComments: number,
  TopicId: number,
  ItemId: number,
  RatingId: number,
  commentUserIds?: string;
  voteSum: number,
  comment?: string,
  isAnonymous: number,
  username: string,
  UserId: number,
  verified: number,
  itemName: string,
  itemCleanName: string,
  releaseYear: string,
  itemTypeName: string,
  itemTypeSlug: string,
  itemTypeId: number,
  isYes: number,
  isVerifiedYes: number,
  hasUserComment: boolean,
  itemId?: number,
  comments: Comment[],
  categoryPref?: CategoryValues;
}

export { statsItem };
